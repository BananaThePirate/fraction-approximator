use std::{fmt::Display, ops::BitAnd};
#[derive(Clone, Copy)]
pub struct Fraction {
    top: u32,
    bottom: u32,
}

impl Fraction {
    pub fn new(top: u32, bottom: u32) -> Fraction {
        Fraction { top, bottom }
    }
    /// converts the fraction to a float by division
    pub fn to_float(self) -> f64 {
        self.top as f64 / self.bottom as f64
    }
    /// uses the farey sequence algorithum to approximate the fraction
    /// *Uses Loops*
    pub fn from_float(number: f64) -> Fraction {
        let integer = number as u32;
        let mut min: Fraction = Fraction::new(integer - 1, 1);
        let mut max: Fraction = Fraction::new(integer + 1, 1);
        loop {
            let mid = min & max;
            let mid_float = mid.to_float();
            if number < mid_float {
                max = mid;
                continue;
            }
            if number > mid_float {
                min = mid;
                continue;
            }
            return mid;
        }
    }
    pub fn simplify(mut self) {
        let gcd = gcd(self.top, self.bottom);
        self.top /= gcd;
        self.bottom /= gcd;
    }
}

// euclid
fn gcd(a: u32, b: u32) -> u32 {
    if b == 0 {
        return a;
    }
    return gcd(b, a % b);
}

/// mediant
impl BitAnd for Fraction {
    type Output = Fraction;
    fn bitand(self, rhs: Self) -> Self::Output {
        Fraction::new(self.top + rhs.top, self.bottom + rhs.bottom)
    }
}

impl Display for Fraction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}/{}", self.top, self.bottom)
    }
}
#[cfg(test)]
mod tests {
    use crate::Fraction;
    #[test]
    fn test() {
        println!("3.14159 = {}", Fraction::from_float(3.14159));
        println!("2.71828 = {}", Fraction::from_float(2.71828));
    }
}
