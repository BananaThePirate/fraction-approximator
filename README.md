# Fraction-approximator

A simple rust fraction approximator using farey sequences

### How it works

The mediant of two fractions is the sum of their numerators divided by the sum their denomonators.

e.g.

$$
\frac{a}{b} \; \textmd{and} \; \frac{c}{d} = \frac{a + c}{c + d}
$$

It is somewhere roughly inbetween both fractions.
if we take a range of values, and find the fractional midpoint, we check if the floating point is in the upper or lower half, 
disregard the useless part, and reiterate.
